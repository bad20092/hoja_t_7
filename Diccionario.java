/**
* @fecha 18/4/2021
* @seccion Algoritmos y Estructuras de Datos 20 
* @class Diccionario
*/

public class Diccionario {
    private String[] ingles;
    private String[] espanol;
    private String[] frances;
    private int tamanoActualDiccionario = 0;
    private final int LIMITE_POR_DEFECTO = 100;
    
    /** Construye un diccionario con capacidad para el limite por defecto.     
     * 
     */
    public Diccionario() {
        ingles = new String[LIMITE_POR_DEFECTO];
        espanol = new String[LIMITE_POR_DEFECTO];
        frances = new String[LIMITE_POR_DEFECTO];

    }
    
    /** Construye un diccionario con la capacidad que se indica como parametro.
     * 
     * @param limite Indica la capacidad máxima del diccionario.
     */
    public Diccionario(int limite) {
        ingles = new String[limite];
        espanol = new String[limite]; 
        frances = new String[limite];       
    }
    
    /** Añade una palabra en español y sus traducciónes a ingles y frances.
     *  En caso de que la palabra española ya esté en el diccionario o que esté
     *  tenga tantas palabras como su capacidad, devuelve false; en otro caso, devuelve
     *  true.     * 
     * 
     * @param e String conteniendo la palabra en español
     * @param i String conteniendo la palabra en ingles
     * @return Boolean indicando si se añadio correctamente la palabra.
     */
    public boolean nuevaEntrada(String e, String i) {
        boolean insertadaCorrectamente = false;
        boolean seEncontroCoincidencia = false;
        
        int j = 0;
        while (!insertadaCorrectamente && j < espanol.length && !seEncontroCoincidencia) {
            if (espanol[j] == null) {
                espanol[j] = e;
                ingles[j] = i;                        
                tamanoActualDiccionario++;
                insertadaCorrectamente = true;
            } 
            else if (espanol[j].equals(e)){
                seEncontroCoincidencia = true;
            }                              
            j++;
        }
        
        return insertadaCorrectamente;
            
    }
    
    
    /** Devuelve el contenido de todo el diccionario formateado en una cadena.
     * 
     * @return Un String con el número de palabras, la capacidad máxima del diccionario 
     * y con cada palabra española y su traducción.
     */
    public String getContenidoCompleto() {
        String diccionario = "El dicccionario tiene actualmente " + tamanoActualDiccionario + " entradas (puede contener hasta " + espanol.length + " entradas)\n";
        
        for (int i = 0; i < tamanoActualDiccionario; i++) {
                diccionario += espanol[i] + " > " + ingles[i] + "\n";                       
        }
        
        return diccionario;        
    }
    
    
    /** Traduce palabraATraducir al idioma de destino
     * 
     * @param palabraATraducir Contiene la palabra a traducir.
     * @param palabrasIdiomaOrigen Array de palabras en el idioma origen.
     * @param palabrasIdiomaDestino Array de palabras en el idioma destino.
     * @return Un String conteniendo la palabra traducida o null si no se encontró
     * traducción.
     */
    private String traduce(String palabraATraducir, String[] palabrasIdiomaOrigen, String[] palabrasIdiomaDestino) {
        String traduccion = null;
        
        palabraATraducir = palabraATraducir.trim();
        
        for (int i = 0; i < tamanoActualDiccionario; i++) {
            if (palabrasIdiomaOrigen[i].equals(palabraATraducir)) {
                traduccion = palabrasIdiomaDestino[i];
            }
        }
        
        return traduccion;
        
    }
    
    
    /** Traduce una única palabra española al ingles.
     * 
     * @param e Contiene la palabra a traducir.
     * @return Un String conteniendo la palabra traducida o null si no se encontró
     * traducción.
     */
    public String traduce2Ingles(String e) {
        return traduce(e, espanol, ingles);
    }

    /** Traduce una única palabra española al frances.
     * 
     * @param e Contiene la palabra a traducir.
     * @return Un String conteniendo la palabra traducida o null si no se encontró
     * traducción.
     */
    public String traduce2Frances(String e) {
        return traduce(e, espanol, frances);
    }
    

     /** Traduce una única palabra inglesa al español.
     * 
     * @param e Contiene la palabra a traducir.
     * @return Un String conteniendo la palabra traducida o null si no se encontró
     * traducción.
     */
    public String traduce2Espanol(String i) {
        return traduce(i, ingles, espanol);
    }
        
    
    /** Traduce un array de palabras al idioma de destino.
     * 
     * @param palabras Array con las palabras a traducir en el idioma origen.
     * @param palabrasIdiomaOrigen Array con las palabras del diccionario en el idioma
     * de origen.
     * @param palabrasIdiomaDestino Array con los palabras del diccionario en el
     * idioma de destino.
     * @return Array conteniendo las palabras traducidas con null en aquellas posiciones
     * en las que no se pudo encontrar una traducción.
     */
    private String[] traduce(String[] palabras, String[] palabrasIdiomaOrigen, String[] palabrasIdiomaDestino) {
        String[] resultado = new String[palabras.length];
        
        for (int i = 0; i < palabras.length; i++) {
            resultado[i] = traduce(palabras[i], palabrasIdiomaOrigen, palabrasIdiomaDestino);
        }
        
        return resultado;
        
    }
    
    /** Traduce un array de palabras en español a ingles y frances. 
     * 
     * @param palabras Array conteniendo las palabras en español para traducir.
     * @return Array conteniendo las palabras traducidas con null en aquellas 
     * posiciones en las que no se pudo encontrar una traducción.
     */
    public String[] traduce2Ingles(String[] palabras) {
        return traduce(palabras, ingles, frances);
    }
    
    
    /** Traduce un array de palabras en inglesas a español y frances.
     * 
     * @param palabras Array conteniendo las palabras inglesas a traducir.
     * @return Array conteniendo las palabras traducidas con null en aquellas 
     * posiciones en las que no se pudo encontrar una traducción.
     */
    public String[] traduce2Espanol(String[] palabras) {
        return traduce(palabras, espanol, frances);
    }
    
    
    /** Busca una palabra en el array listadoPalabras.
     * 
     * @param palabra String conteniendo la palabra base de la búsquyeda.
     * @param listadoPalabras Array conteniendo las palabras del diccionario entre 
     * las que buscar alguna parecida a palabra.
     * @return Un String conteniendo una palabra parecida a palabra o null en caso
     * de no encontrar ninguna.
     */
    public String sugerirPalabra(String palabra, String[] listadoPalabras) {
        String resultado = null;      
        
        palabra = palabra.trim();
        int longitudDeLaPalabra = palabra.length();
        
        for (int i = 0; i < tamanoActualDiccionario; i++) {
            String palabraAComparar = listadoPalabras[i];
                                               
            if (longitudDeLaPalabra == palabraAComparar.length()) {
                int numeroLetrasDiferentes = 0;               
                
                for (int j = 0; j < longitudDeLaPalabra; j++) {
                    if (palabra.charAt(j) != palabraAComparar.charAt(j))
                        numeroLetrasDiferentes++;                                        
                }
                
                if (numeroLetrasDiferentes == 1)
                    resultado = palabraAComparar;
            }                                                 
        }
        
        return resultado;
    }
        
        
    /** Busca una palabra en el conjunto de palabras inglesas del
     * diccionario.
     * 
     * @param palabra String conteniendo la palabra base de la búsqueda.
     * @return Un String conteniendo una palabra parecida a palabra o null en caso
     * de no encontrar ninguna.
     */
    public String sugerirPalabraIngles(String palabra) {
        return sugerirPalabra(palabra, ingles);
    }
    
        
    /** Busca una palabra en el conjunto de palabras en español del
     * diccionario.
     * 
     * @param palabra String conteniendo la palabra base de la búsqueda.
     * @return Un String conteniendo una palabra parecida a palabra o null en caso
     * de no encontrar ninguna.
     */    
    public String sugerirPalabraEspanol(String palabra) {
        return sugerirPalabra(palabra, espanol);
    }
              
}
